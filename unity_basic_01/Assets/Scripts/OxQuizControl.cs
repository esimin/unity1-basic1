﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OxQuizControl : MonoBehaviour {

	public Text mainText;

	public string question = "숫자 5 는 ??";
	public bool answer = false;

	// Use this for initialization
	void Start () {

		mainText.text = question;
		
	}
	
	// Update is called once per frame
	void Update () {
		}

	public void QuizInput(bool inp){

		if (inp == answer) {
			mainText.text = "Right!!";
		} else {
			mainText.text = "Wrong!!";
		}
		}

	public void pressOxButton(){
		SceneManager.LoadScene ("OxQuiz");
		}
}