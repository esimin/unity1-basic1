﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

	public Text mainText;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void pressTheButton(){
		for (int i = 1; i <= 9; i++) {
			for (int j = 1; j <= 9; j++) {

				string str = i + " * " + j + " = " + (i * j) + " ";
				mainText.text = mainText.text + str;

			}
			mainText.text = mainText.text + "\n";
		}
	}
		
}
	